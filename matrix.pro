% Copyright (C) 2013, Cameron White
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
% 3. Neither the name of the project nor the names of its contributors
%    may be used to endorse or promote products derived from this software
%    without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
% OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
% OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
% SUCH DAMAGE.

mget([H|_], 0, J, X) :-
	vget(H, J, X).
mget([_|T], I, J, X) :-
	I > 0,
	I1 is I - 1,
	mget(T, I1, J, X).

mscale([], _, []).
mscale([H1|T1], X, [H2|T2]) :-
    vscale(H1, X, H2),
    mscale(T1, X, T2).

mvmul([], _, []).
mvmul([Ms|Mss], Us, [V|Vs]) :-
	dot(Ms, Us, V),
	mvmul(Mss, Us, Vs).

numberOfRows(Matrix, Rows) :-
	length(Matrix, Rows).

numberOfColumns([Vector|_], Columns) :-
	length(Vector, Columns).

determinant(Matrix, Determinant) :-
	numberOfRows(Matrix, Rows),
	numberOfColumns(Matrix, Columns),
	Rows = 2,
	Columns = 2,
	mget(Matrix, 0, 0, A),
	mget(Matrix, 0, 1, B),
	mget(Matrix, 1, 0, C),
	mget(Matrix, 1, 1, D),
	Determinant is A * D - B * C.

determinant(Matrix, Determinant) :-
	numberOfRows(Matrix, Rows),
	numberOfColumns(Matrix, Columns),
	Rows = Columns,
	subMatrices(Matrix, Columns, Subs),
	determinants(Subs, Determinant).

determinants([], 0).
determinants([M|Ms], D) :-
	determinants(Ms, D0),
	determinant(M, D1),
	D is D0 + D1.

subMatrix(Matrix, I, J, SubMatrix) :-
	removeRow(Matrix, I, SubMatrix0),
	removeColumn(SubMatrix0, J, SubMatrix).

subMatrices(_, 0, []).
subMatrices(Matrix, N, [Sub|Subs]) :-
	N > 0,
	N1 is N - 1,
	subMatrix(Matrix, 0, N1, Sub),
	subMatrices(Matrix, N1, Subs).

removeRow(Matrix, I, X) :-
	removeElement(Matrix, I, X).

removeColumn([], _, []).
removeColumn([Us|Uss], I, [Vs|Vss]) :-
	removeElement(Us, I, Vs),
	removeColumn(Uss, I, Vss).

getRow([X|_], 0, X).
getRow([_|T], I, X) :-
	I > 0,
	I1 is I-1,
	getRow(T, I1, X).

getColumn([], _, []).
getColumn([Ms|Mss], I, [U|Us]) :-
	vget(Ms, I, U),
	getColumn(Mss, I, Us).
